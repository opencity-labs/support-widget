import PocketBase from "https://cdnjs.cloudflare.com/ajax/libs/pocketbase/0.21.1/pocketbase.es.mjs";

window.closeSupportWidget = function () {
  const elem = document.getElementById("supportwidget-frame-wrapper");
  elem.remove();
};
window.openSupportWidget = function () {
  document.getElementById("widget-support-iframe-container").insertAdjacentHTML(
    "beforeend",
    `<div id="supportwidget-frame-wrapper" class="frame-right" style="background: #eee; background: linear-gradient(110deg, #ececec 8%, #f5f5f5 18%, #ececec 33%); background-size: 400% 100%; animation: 3s shine linear infinite; max-width: calc(100% - 34px); border-radius: 12px; z-index: 2147483647; width: 384px; position: fixed; right: 30px; bottom: 68px; bottom: calc(20px + 80px); height: calc(100% - 84px); min-height: 288px; max-height: 624px; box-shadow: 0 5px 40px rgba(0, 0, 0, 0.16); transition: height 0.3s ease-in; overflow: hidden !important; right: 30px; bottom: 80px; height: calc(100% - 84px); visibility: visible;">
    <div onClick="window.closeSupportWidget()" id="support-widget-close" style="cursor: pointer;position: absolute;right: 16px;top: 10px;width: 32px;" role="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" xml:space="preserve" enable-background="new 0 0 24 24" role="img" style="color:white"><title>Chiudi</title><path d="m12.7 12 3.7 3.6-.8.8-3.6-3.7-3.6 3.7-.8-.8 3.7-3.6-3.7-3.6.8-.8 3.6 3.7 3.6-3.7.8.8-3.7 3.6z" fill="#ffffff"></path></svg></div>
    <iframe allow="display-capture" src="${window.OC_SUPPORT_WIDGET_BASE_URL}/?${window.supportWidgetParams}" id="widget-frame" data-testid="widget-frame" style="border: none; width: 100%; height: 100%; border-radius: inherit; background-color"></iframe></div>`
  );
};
window.onSupportWidgetBtnClick = function () {
  const elem = document.getElementById("supportwidget-frame-wrapper");
  if (elem) {
    elem.remove();
  } else {
    window.openSupportWidget();
  }
};
const html = `
<style type="text/css">
  @keyframes shine {
    to {
      background-position-x: -400%;
    }
  }
</style>
<div
  data-html2canvas-ignore="true"
  style="
    width: 0px;
    height: 0px;
    bottom: 0px;
    right: 0px;
    z-index: 2147483647;
  "
>
  <div id="widget-support-iframe-container" aria-live="polite">
    <div id="supportwidget-btn"
    class="p-2"><button
    onclick="window.onSupportWidgetBtnClick()"
    type="button"
    style="right: 22px;
      bottom: 22px;
      border: none;
      position: fixed;
      min-width: 104px;
      max-width: 156px;
      height: 40px;
      z-index: 2147483000;
      visibility: visible;
      font-family: 'Titillium Web', Geneva, Tahoma, sans-serif;
      display: flex;
      -webkit-box-align: center;
      align-items: center;
      float: right;
      border-radius: 6px;
      background-color: #17334f;
      box-shadow: none;
      color: rgb(255, 255, 255);
      z-index: 2147483647;
      cursor: pointer;
      padding: 8px;
      font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
      -webkit-font-smoothing"
    class="btn-icon btn btn-dark btn-xs" data-focus-mouse="false"><svg style="fill: rgb(255, 255, 255); width: 16px; height: 16px; float: left; margin-left: 1px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" xml:space="preserve" enable-background="new 0 0 24 24" class="icon icon-white" role="img"><title>help</title><path d="M12 2C6.5 2 2 6.5 2 12s4.5 10 10 10 10-4.5 10-10S17.5 2 12 2zm0 19c-5 0-9-4-9-9s4-9 9-9 9 4 9 9-4 9-9 9zm-1.2-4.5H12v1.8h-1.2v-1.8zm4.5-8c0 .6-.1 1.2-.3 1.8-.3.5-.8 1-1.3 1.4L12.3 13c-.2.4-.4.8-.4 1.2v.6H11c-.1-.3-.2-.7-.2-1 0-.4.2-.9.5-1.2.4-.5.9-1 1.4-1.4.5-.4.9-.8 1.2-1.3.2-.4.3-.9.3-1.4 0-.5-.2-1.1-.6-1.4-.6-.3-1.3-.5-2-.4L9.3 7h-.5v-.8c1-.3 2-.5 3-.5.9-.1 1.9.1 2.7.6.6.6.9 1.4.8 2.2z"></path></svg> Chiedi aiuto</button></div>

  </div>
</div>`;
const pb = new PocketBase("https://manager.opencityitalia.it/v1");
const parseJwt = (token) => {
  try {
    return JSON.parse(atob(token.split(".")[1]));
  } catch (e) {
    return null;
  }
};
function handleResponse(response) {
  return response.text().then((text) => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }
    return data;
  });
}
function getBaseUrl() {
  if (window.OC_BASE_URL) {
    return window.OC_BASE_URL;
  } else if (/\/lang\/(it|en|de)\//.test(window.location.href)){
    const langIndex = window.location.href.search(/\/lang\/(it|en|de)\//);
    if (langIndex !== -1) {
        return window.location.href.substring(0, langIndex) + '/lang';
    }
  } else if (/\/(it|en|de)\//.test(window.location.href)){
    const langIndex = window.location.href.search(/\/(it|en|de)\//);
    if (langIndex !== -1) {
        return window.location.href.substring(0, langIndex);
    }
  }
}
function getToken() {
  const baseUrl = getBaseUrl()
  const requestOptions = {
    method: "GET",
  };
  return fetch(
    `${baseUrl}/api/session-auth?with-cookie=true`,
    requestOptions
  ).then(handleResponse);
}
function renderWidget(token) {
  const session = parseJwt(token);
  pb.collection("sdc_tenants")
    .getFullList({
      filter: `uuid = "${session?.tenant_id || window.OC_TENANT_UUID}"`,
      expand: "owner",
      fields:
        "stack,expand.owner.name,expand.owner.support_type,expand.owner.support_email,expand.owner.support_endpoint",
    })
    .then(async (result) => {
      let items = result ? result[0]?.expand?.owner : {};
      if (
        items?.support_type === "freshdesk" ||
        items?.support_type === "email"
      ) {
        items = {
          ...items,
          stack: result[0]?.stack,
          tenantUuid: window.OC_TENANT_UUID,
          akFreshdesk: window.OC_AK_FRESHDESK,
          parentHref: window.location.href,
          username: session?.username,
          tenant_id: session?.tenant_id,
          roles: session?.roles,
        };
        window.supportWidgetParams = new URLSearchParams(items).toString();
        console.log(window.supportWidgetParams);
        document.body.insertAdjacentHTML("afterend", html);
      }
    });
}
getToken()
  .then((response) => {
    const token = response.token;
    renderWidget(token);
  })
  .catch((err) => {
    renderWidget();
  });
